import http from 'k6/http';
import { sleep, check } from 'k6';


export const options = {
    // hosts: { 'test.k6.io': '1.2.3.4' },
    stages: [

      { duration: '1m', target: 50 },
      { duration: '1m', target: 100 },
      { duration: '1m', target: 150 },
      { duration: '30s', target: 0 },
    ],
    thresholds: { http_req_duration: ['p(90)<1000'] },
    // noConnectionReuse: true,
    // userAgent: 'MyK6UserAgentString/1.0',
  };

export default function () {
    let response = http.post(
        `http://34.96.121.181/auth/login`,
        JSON.stringify({
            "email": "michael@fragrant-chios.com",
            "password": "password",
            "scopes": "me:read competitions:read competitions:write competitions:update competitions:delete associations:read associations:write associations:update associations:delete"
        })
    )
    check(response, {
        "login response code was 200": (res) => res.status == 200,
    })

    const access_token = response.cookies.access_token[0].value
    const refresh_token = response.cookies.refresh_token[0].value

    response = http.post(
        `http://34.96.121.181/auth/verify?required_scopes=me:read`,
        JSON.stringify({
            "email": "michael@fragrant-chios.com",
            "password": "password",
            "scopes": "me:read competitions:read competitions:write competitions:update competitions:delete associations:read associations:write associations:update associations:delete"
        }),
        {
            headers: {
                "Authorization": `Bearer ${access_token}`
            }
        }
    )
    check(response, {
        "verify response code was 200": (res) => res.status == 200,
    })

    response = http.get(
        `http://34.96.121.181/users/me`,
        {
            headers: {
                "Authorization": `Bearer ${access_token}`
            }
        }
    )
    check(response, {
        "me response code was 200": (res) => res.status == 200,
    })

    response = http.post(
        `http://34.96.121.181/auth/logout`,
        JSON.stringify({
            "refresh_token": refresh_token,
        }),
        {
            headers: {
                "Authorization": `Bearer ${access_token}`
            }
        }
    )
    check(response, {
        "verify response code was 200": (res) => res.status == 200,
    })

    sleep(30);
}
