register_parameters = [
    (
        {},
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "email": "field required",
                        "first_name": "field required",
                        "last_name": "field required",
                        "password": "field required",
                        "consent": "field required",
                    },
                },
            },
        },
    ),
    (
        {
            "first_name": None,
            "last_name": None,
            "email": None,
            "password": None,
            "consent": None,
        },
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "email": "none is not an allowed value",
                        "first_name": "none is not an allowed value",
                        "last_name": "none is not an allowed value",
                        "password": "none is not an allowed value",
                        "consent": "none is not an allowed value",
                    },
                },
            },
        },
    ),
]

login_parameters = [
    (
        {},
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "email": "field required",
                        "password": "field required",
                        "scopes": "field required",
                    },
                },
            },
        },
    ),
    (
        {"email": None, "password": None, "scopes": None},
        {
            "status": 400,
            "response": {
                "detail": {
                    "errors": {
                        "email": "none is not an allowed value",
                        "password": "none is not an allowed value",
                        "scopes": "none is not an allowed value",
                    },
                },
            },
        },
    ),
    (
        {
            "email": "invalidformat",  # oauth expects username, we cannot check email format
            "password": "invalid",
            "scopes": "me:read",
        },
        {
            "status": 401,
            "response": {
                "detail": {"errors": {"credentials": "incorrect email or password"}}
            },
        },
    ),
    (
        {
            "email": "invalid@email.com",
            "password": "password",
            "scopes": "me:read",
        },
        {
            "status": 401,
            "response": {
                "detail": {"errors": {"credentials": "incorrect email or password"}}
            },
        },
    ),
    (
        {
            "email": "michael@fragrant-chios.com",
            "password": "invalid",
            "scopes": "me:read",
        },
        {
            "status": 401,
            "response": {
                "detail": {"errors": {"credentials": "incorrect email or password"}}
            },
        },
    ),
    (
        {
            "email": "invalid@email.com",
            "password": "invalid",
            "scopes": "me:read",
        },
        {
            "status": 401,
            "response": {
                "detail": {"errors": {"credentials": "incorrect email or password"}}
            },
        },
    ),
]
