from fastapi.testclient import TestClient
from sqlalchemy.orm import Session


def test_me_unauthorized(build_db, db: Session, client: TestClient):
    response = client.get(url="/users/me")

    assert response.status_code == 401
    assert response.json() == {"detail": "missing authorization header"}


def test_me_insufficient_permissions(
    build_db, db: Session, client: TestClient, account, login
):
    access_token, refresh_token = login(account.email, "password", "")

    response = client.get(
        url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
    )

    assert response.status_code == 401
    assert response.json() == {
        "detail": {"errors": {"scopes": "insufficient permissions"}}
    }


def test_me_wrong_permissions(
    build_db, db: Session, client: TestClient, account, login
):
    access_token, refresh_token = login(account.email, "password", "competitions:read")

    response = client.get(
        url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
    )

    assert response.status_code == 401
    assert response.json() == {
        "detail": {"errors": {"scopes": "insufficient permissions"}}
    }


def test_me_sufficient_permissions(
    build_db, db: Session, client: TestClient, account, login
):
    access_token, refresh_token = login(account.email, "password")

    response = client.get(
        url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
    )

    assert response.status_code == 200
    assert response.json() == {
        "id": str(account.id),
        "first_name": account.first_name,
        "last_name": account.last_name,
        "email": account.email,
        "consent": account.consent,
        "created_at": account.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "updated_at": account.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
    }
