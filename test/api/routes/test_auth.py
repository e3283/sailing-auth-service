import datetime

import pytest
import pytz
from fastapi.testclient import TestClient
from freezegun.api import freeze_time
from sqlalchemy.orm import Session

from api.models.user import User

from . import login_parameters, register_parameters


@pytest.mark.parametrize("params, expected_response", register_parameters)
def test_register_params(
    build_db, client: TestClient, params: dict, expected_response: dict
):
    response = client.post(
        url="/auth/register",
        json=params,
    )

    assert response.status_code == expected_response["status"]
    assert response.json() == expected_response["response"]


def test_register_with_same_email_twice(build_db, client: TestClient):
    response = client.post(
        url="/auth/register",
        json={
            "first_name": "Michael",
            "last_name": "Groenewegen van der Weijden",
            "email": "michael@fragrant-chios.com",
            "password": "password",
            "consent": True,
        },
    )

    assert response.status_code == 200

    response = client.post(
        url="/auth/register",
        json={
            "first_name": "Michael",
            "last_name": "Groenewegen van der Weijden",
            "email": "michael@fragrant-chios.com",
            "password": "password",
            "consent": True,
        },
    )

    assert response.status_code == 400
    assert response.json() == {"detail": {"errors": {"email": "email already in use"}}}


def test_successful_registration(build_db, db: Session, client: TestClient):
    response = client.post(
        url="/auth/register",
        json={
            "first_name": "Michael",
            "last_name": "Groenewegen van der Weijden",
            "email": "michael@fragrant-chios.com",
            "password": "password",
            "consent": True,
        },
    )

    assert response.status_code == 200

    user = db.query(User).first()

    assert response.json() == {
        "id": str(user.id),
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "consent": user.consent,
        "created_at": user.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        "updated_at": user.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
    }


@pytest.mark.parametrize("params, expected_response", login_parameters)
def test_login_params(
    build_db, client: TestClient, account, params: dict, expected_response: dict
):
    response = client.post(url="/auth/login", json=params)

    assert response.status_code == expected_response["status"]
    assert response.json() == expected_response["response"]


def test_successful_login(build_db, client: TestClient, account):
    response = client.post(
        url="/auth/login",
        json={"email": account.email, "password": "password", "scopes": "me:read"},
    )

    assert response.status_code == 200
    assert response.cookies.get("access_token")
    assert response.cookies.get("refresh_token")


def test_expired_access_token(build_db, client: TestClient, login, account):
    initial_datetime = datetime.datetime.now(tz=pytz.utc)
    with freeze_time(initial_datetime) as frozen_datetime:
        access_token, refresh_token = login(account.email, "password")

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 200
        assert response.json() == {
            "id": str(account.id),
            "first_name": account.first_name,
            "last_name": account.last_name,
            "email": account.email,
            "consent": account.consent,
            "created_at": account.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
            "updated_at": account.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        }

        frozen_datetime.tick(delta=datetime.timedelta(hours=2))

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 401
        assert response.json() == {"detail": "signature has expired"}


def test_expired_refresh_token(build_db, client: TestClient, login, account):
    initial_datetime = datetime.datetime.now(tz=pytz.utc)
    with freeze_time(initial_datetime) as frozen_datetime:
        access_token, refresh_token = login(account.email, "password")

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 200
        assert response.json() == {
            "id": str(account.id),
            "first_name": account.first_name,
            "last_name": account.last_name,
            "email": account.email,
            "consent": account.consent,
            "created_at": account.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
            "updated_at": account.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        }

        frozen_datetime.tick(delta=datetime.timedelta(days=20))

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 401
        assert response.json() == {"detail": "signature has expired"}

        response = client.post(
            url="/auth/refresh", headers={"Authorization": f"Bearer {refresh_token}"}
        )

        assert response.status_code == 401
        assert response.json() == {"detail": "signature has expired"}
        assert not response.cookies.get("access_token")


def test_refresh_access_token(build_db, client: TestClient, login, account):
    initial_datetime = datetime.datetime.now(tz=pytz.utc)
    with freeze_time(initial_datetime) as frozen_datetime:
        access_token, refresh_token = login(account.email, "password")

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 200
        assert response.json() == {
            "id": str(account.id),
            "first_name": account.first_name,
            "last_name": account.last_name,
            "email": account.email,
            "consent": account.consent,
            "created_at": account.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
            "updated_at": account.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        }

        frozen_datetime.tick(delta=datetime.timedelta(hours=2))

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 401
        assert response.json() == {"detail": "signature has expired"}

        response = client.post(
            url="/auth/refresh", headers={"Authorization": f"Bearer {refresh_token}"}
        )

        assert response.status_code == 200
        assert response.cookies.get("access_token")
        assert access_token != response.cookies.get("access_token")

        access_token = response.cookies.get("access_token")

        response = client.get(
            url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == 200
        assert response.json() == {
            "id": str(account.id),
            "first_name": account.first_name,
            "last_name": account.last_name,
            "email": account.email,
            "consent": account.consent,
            "created_at": account.created_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
            "updated_at": account.updated_at.strftime("%Y-%m-%dT%H:%M:%S.%f+00:00"),
        }


def test_verify_valid_token(build_db, client: TestClient, login, account):
    access_token, refresh_token = login(account.email, "password")

    response = client.post(
        url="/auth/verify?required_scopes=me:read",
        headers={"Authorization": f"Bearer {access_token}"},
    )

    assert response.status_code == 200
    assert response.json() == {"sub": "michael@fragrant-chios.com"}


def test_verify_expired_token(build_db, client: TestClient, login, account):
    initial_datetime = datetime.datetime.now(tz=pytz.utc)
    with freeze_time(initial_datetime) as frozen_datetime:
        access_token, refresh_token = login(account.email, "password")

        frozen_datetime.tick(delta=datetime.timedelta(hours=2))

        response = client.post(
            url="/auth/verify?required_scopes=me:read",
            headers={"Authorization": f"Bearer {access_token}"},
        )

        assert response.status_code == 401
        assert response.json() == {"detail": "signature has expired"}


def test_verify_invalid_token(build_db, client: TestClient, login, account):
    access_token, refresh_token = login(account.email, "password")

    response = client.post(
        url="/auth/verify?required_scopes=me:read",
        headers={"Authorization": "Bearer invalidtoken"},
    )

    assert response.status_code == 401
    assert response.json() == {"detail": "not enough segments"}


def test_verify_no_scopes(build_db, client: TestClient, login, account):
    access_token, refresh_token = login(account.email, "password")

    response = client.post(
        url="/auth/verify",
        headers={"Authorization": f"Bearer {access_token}"},
    )

    assert response.status_code == 401
    assert response.json() == {
        "detail": {"errors": {"scopes": "insufficient permissions"}}
    }


def test_verify_insufficient_permissions_token(
    build_db, client: TestClient, login, account
):
    access_token, refresh_token = login(account.email, "password", "")

    response = client.post(
        url="/auth/verify?required_scopes=me:read",
        headers={"Authorization": f"Bearer {access_token}"},
    )

    assert response.status_code == 401
    assert response.json() == {
        "detail": {"errors": {"scopes": "insufficient permissions"}}
    }


def test_me_deleted_user(build_db, db: Session, client: TestClient, login, account):
    access_token, refresh_token = login(account.email, "password")

    db.delete(account)
    db.commit()

    response = client.get(
        url="/users/me", headers={"Authorization": f"Bearer {access_token}"}
    )

    assert response.status_code == 401
    assert response.json() == {
        "detail": {"error": {"credentials": "invalid credentials"}}
    }
