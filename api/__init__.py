from fastapi import FastAPI, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from passlib.context import CryptContext

from api.caching import get_cache
from api.config import settings
from api.database import Base, get_db

app = FastAPI()

scopes = {
    "me:read": "read personal data.",
    "competitions:read": "read all or an individual competition(s).",
    "competitions:write": "create a new competition(s).",
    "competions:update": "update an existing competion(s)",
    "competitions:delete": "delete an existing competition(s).",
    "associations:read": "read all or an individual association(s).",
    "associations:write": "create a new association(s).",
    "associations:update": "update an existing association(s)",
    "associations:delete": "delete an existing association(s).",
}

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def create_app():
    """Intialize the application."""

    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.allowed_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    # register the api endpoints
    from api.routes import auth

    app.include_router(auth.router)

    from api.routes import user

    app.include_router(user.router)

    @AuthJWT.load_config
    def get_config():
        return settings

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(
        request: Request, exc: RequestValidationError
    ):
        errors = dict()
        for error in exc.errors():
            errors[error["loc"][-1]] = error["msg"]

        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content=jsonable_encoder({"detail": {"errors": errors}}),
        )

    @app.exception_handler(AuthJWTException)
    def authjwt_exception_handler(request: Request, exc: AuthJWTException):
        return JSONResponse(
            status_code=status.HTTP_401_UNAUTHORIZED,
            content={"detail": exc.message.lower()},
        )

    @app.get("/health-check", status_code=status.HTTP_200_OK)
    def health_check():
        return {"healthy": True}

    return app
