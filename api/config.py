from typing import List

from pydantic import BaseSettings


class Settings(BaseSettings):
    # app settings
    fastapi_app: str
    fastapi_port: int

    allowed_origins: List[str]

    # database settings
    database_hostname: str
    database_port: str
    database_password: str
    database_name: str
    database_username: str

    # cache settings
    cache_hostname: str
    cache_port: int
    cache_password: str = None
    cache_name: int = 0
    cache_username: str = None

    # broker settings
    broker_hostname: str
    broker_port: str
    broker_password: str
    broker_vhost: str
    broker_username: str

    broker_exchange: str
    broker_route: str

    # password settings
    hash_algorithm: str

    # token settings
    authjwt_secret_key: str
    access_token_expire_minutes: int
    refresh_token_expire_minutes: int

    class Config:
        env_file = ".env"


settings = Settings()
