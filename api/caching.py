import redis

from api.config import settings

redis_client = redis.Redis(
    decode_responses=True,
    host=settings.cache_hostname,
    port=settings.cache_port,
    client_name=settings.cache_username,
    password=settings.cache_password,
    db=settings.cache_name,
)


def get_cache():
    try:
        yield redis_client
    finally:
        pass
