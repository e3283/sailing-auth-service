import json

from aio_pika import DeliveryMode, ExchangeType, Message, connect_robust

from api.config import settings


async def publish(message: dict, to: str) -> None:
    connection = await connect_robust(
        f"amqp://{settings.broker_username}:{settings.broker_password}@{settings.broker_hostname}/{settings.broker_vhost}"
    )

    async with connection:
        channel = await connection.channel()
        exchange = await channel.declare_exchange(
            name=settings.broker_exchange, type=ExchangeType.DIRECT
        )

        queue = await channel.declare_queue(name="competition-service")

        await queue.bind(exchange=exchange, routing_key="user.register")
        await queue.bind(exchange=exchange, routing_key="user.update")
        await queue.bind(exchange=exchange, routing_key="user.delete")

        await exchange.publish(
            message=Message(
                body=json.dumps(message, ensure_ascii=False).encode("gbk"),
                delivery_mode=DeliveryMode.PERSISTENT,
            ),
            routing_key=to,
        )
