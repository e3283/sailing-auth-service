import json
import os
from glob import glob

from fastapi import APIRouter, BackgroundTasks, Depends, Response, Security, status
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session

from api.database import get_db
from api.models import User
from api.producer import publish
from api.schemas import UserOut
from api.services import get_current_user

router = APIRouter(prefix="/users", tags=["User"])


@router.get("/me", response_model=UserOut, status_code=status.HTTP_200_OK)
async def me(current_user: UserOut = Security(get_current_user, scopes=["me:read"])):
    """Return the personal data of a user."""
    return current_user


# @router.put("/me", response_model=UserOut, status_code=status.HTTP_200_OK)
# async def update(
#     updated_user: UserUpdate,
#     background_tasks: BackgroundTasks,
#     current_user: User = Security(get_current_user, scopes=["me:update"]),
#     db: Session = Depends(get_db),
# ):
#     """Update the personal data of a user."""

#     user_query = db.query(User).filter(User.email == current_user.email)

#     # update the competition with the new provided data
#     updated_user = user_query.update(updated_user.dict(), synchronize_session=False)
#     db.commit()
#     db.refresh(current_user)

#     background_tasks.add_task(
#         publish,
#         message=dict(
#             first_name=current_user.first_name,
#             last_name=current_user.last_name,
#             email=current_user.email,
#         ),
#         to="user.update",
#     )

#     return current_user


@router.delete("/me", status_code=status.HTTP_204_NO_CONTENT)
async def delete(
    background_tasks: BackgroundTasks,
    current_user: User = Security(get_current_user, scopes=["me:update"]),
    db: Session = Depends(get_db),
):
    """Delete the personal data of a user."""

    user_email: str = current_user.email

    db.delete(current_user)
    db.commit()

    background_tasks.add_task(
        publish,
        message=dict(email=user_email),
        to="user.delete",
    )

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/me/information")
async def me_information(
    current_user: UserOut = Security(get_current_user, scopes=["me:read"])
):
    """Return the personal data of a user as json file."""

    file_list = glob(os.path.join("../../userinfo", "*"))
    for file in file_list:
        os.remove(file)

    with open(
        f"./userinfo/{current_user.first_name}_{current_user.last_name}.json", "w"
    ) as file:
        file.write(
            json.dumps(
                dict(
                    id=str(current_user.id),
                    first_name=current_user.first_name,
                    last_name=current_user.last_name,
                    email=current_user.email,
                    consent=current_user.consent,
                    created_at=str(current_user.created_at),
                    updated_at=str(current_user.updated_at),
                )
            )
        )

    return FileResponse(
        f"./userinfo/{current_user.first_name}_{current_user.last_name}.json",
        filename=f"{current_user.first_name}_{current_user.last_name}.json",
    )
