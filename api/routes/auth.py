import datetime
import random
import string
import time

import jwt
from fastapi import (
    APIRouter,
    BackgroundTasks,
    Depends,
    HTTPException,
    Request,
    Response,
    status,
)
from fastapi_jwt_auth import AuthJWT
from redis import Redis
from sqlalchemy.orm import Session

from api import get_cache, get_db, settings
from api.models import User
from api.producer import publish
from api.schemas import Credentials, RefreshToken, UserCreate, UserOut
from api.services import get_password_hash, validate_scopes, verify_password

router = APIRouter(prefix="/auth", tags=["Authorization"])


@router.post("/register", response_model=UserOut, status_code=status.HTTP_200_OK)
async def register(
    user: UserCreate, background_tasks: BackgroundTasks, db: Session = Depends(get_db)
):
    """Handle registration of a new user."""

    if db.query(User).filter_by(email=user.email).first():
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail={"errors": {"email": "email already in use"}},
        )

    if not user.consent:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail={"errors": {"consent": "consent to privacy policy to use system"}},
        )

    # salt = bcrypt.gensalt()
    salt = "".join(
        random.choice(string.ascii_letters + string.digits) for _ in range(128)
    )

    user.password = get_password_hash(f"{salt}{user.password}")

    new_user = User(**user.dict(), salt=salt)
    db.add(new_user)
    db.commit()
    db.refresh(new_user)

    background_tasks.add_task(
        publish,
        message=user.dict(exclude={"password"}),
        to=settings.broker_route,
    )

    return new_user


@router.post("/login", status_code=status.HTTP_200_OK)
async def login(
    credentials: Credentials,
    response: Response,
    Authorize: AuthJWT = Depends(),
    db: Session = Depends(get_db),
):
    """Handle login of a registered user."""
    user: User = db.query(User).filter(User.email == credentials.email).first()

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail={"errors": {"credentials": "incorrect email or password"}},
        headers={"WWW-Authenticate": "Bearer"},
    )

    if not user:
        raise credentials_exception

    if not verify_password(f"{user.salt}{credentials.password}", user.password):
        raise credentials_exception

    # create access token and refresh token
    access_token = Authorize.create_access_token(
        subject=user.email,
        user_claims=dict(scopes=credentials.scopes.split(" ")),
        expires_time=datetime.timedelta(minutes=settings.access_token_expire_minutes),
    )
    refresh_token = Authorize.create_refresh_token(
        subject=user.email,
        user_claims=dict(scopes=credentials.scopes.split(" ")),
        expires_time=datetime.timedelta(minutes=settings.refresh_token_expire_minutes),
    )

    # set access token and refresh token in a cookie
    response.set_cookie(
        key="access_token",
        value=access_token,
        max_age=settings.access_token_expire_minutes * 60,
    )
    response.set_cookie(
        key="refresh_token",
        value=refresh_token,
        max_age=settings.refresh_token_expire_minutes * 60,
    )

    return {"message": "login successful"}


@router.post("/logout", status_code=status.HTTP_200_OK)
async def logout(
    token: RefreshToken,
    request: Request,
    response: Response,
    Authorize: AuthJWT = Depends(),
    redis: Redis = Depends(get_cache),
):
    """Handle logout of a logedin user."""
    Authorize.jwt_required()
    access_token = request.headers.get("authorization").split()[1]

    if not (refresh_token := token.refresh_token):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail={"errors": {"refresh_token": "did not provide refresh token."}},
        )

    # decode the access token to get the expiration time
    access_token_decoded = jwt.decode(jwt=access_token, key=settings.authjwt_secret_key)

    # decode the refresh token to get the expiration time
    refresh_token_decoded = jwt.decode(
        jwt=refresh_token, key=settings.authjwt_secret_key
    )

    redis.setex(
        name=access_token,
        time=int(access_token_decoded["exp"] - int(time.time())) * 1000,
        value=access_token,
    )
    redis.setex(
        name=refresh_token,
        time=int(refresh_token_decoded["exp"] - int(time.time())) * 1000,
        value=refresh_token,
    )

    # expire access token and refresh token
    response.delete_cookie(key="access_token")
    response.delete_cookie(key="refresh_token")

    return {"message": "logout succesful"}


@router.post("/refresh", status_code=status.HTTP_200_OK)
async def refresh(
    request: Request,
    response: Response,
    Authorize: AuthJWT = Depends(),
    redis: Redis = Depends(get_cache),
):
    """Handle refreshing of a user's token."""

    Authorize.jwt_refresh_token_required()

    refresh_token = request.headers.get("authorization").split()[1]

    if redis.get(name=refresh_token):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    current_user = Authorize.get_jwt_subject()
    scopes = Authorize.get_raw_jwt()["scopes"]

    new_access_token = Authorize.create_access_token(
        subject=current_user, user_claims=dict(scopes=scopes)
    )
    response.set_cookie(
        key="access_token",
        value=new_access_token,
        max_age=settings.access_token_expire_minutes * 60,
    )

    return {"message": "refresh successful"}


@router.post("/verify", status_code=status.HTTP_200_OK)
async def verify(
    request: Request,
    required_scopes: str = "",
    Authorize: AuthJWT = Depends(),
    redis: Redis = Depends(get_cache),
):
    """Handle verifing the validity of a token."""
    Authorize.jwt_required()

    access_token = request.headers.get("authorization").split()[1]

    if redis.get(name=access_token):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    if required_scopes:
        authentication_value = f"Bearer scope={required_scopes}"
    else:
        authentication_value = "Bearer"

    current_user = Authorize.get_jwt_subject()
    token_scopes = Authorize.get_raw_jwt()["scopes"]

    if not validate_scopes(
        required_scopes=required_scopes.split(" "), token_scopes=token_scopes
    ):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail={"errors": {"scopes": "insufficient permissions"}},
            headers={"WWW-Authenticate": authentication_value},
        )

    return {"sub": current_user}
