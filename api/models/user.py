import sqlalchemy as sa

from api import Base


class User(Base):
    """Representing a user as a database table."""

    first_name = sa.Column(sa.Text, nullable=False)
    last_name = sa.Column(sa.Text, nullable=False)
    email = sa.Column(sa.Text, nullable=False, unique=True, index=True)
    salt = sa.Column(sa.Text, nullable=False)
    password = sa.Column(sa.Text, nullable=False)
    consent = sa.Column(sa.Boolean, nullable=False, default=False)
