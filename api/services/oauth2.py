from typing import List

from fastapi import Depends, HTTPException, Request, status
from fastapi_jwt_auth import AuthJWT
from redis import Redis
from sqlalchemy.orm import Session

from api import get_db, pwd_context
from api.caching import get_cache
from api.models import User


def verify_password(plain_password: str, hashed_password: str):
    """Utility function to verify if a received password matches the hash stored."""
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str):
    """Utility function to hash a password coming from the user."""
    return pwd_context.hash(password)


def validate_scopes(required_scopes: List[str], token_scopes: List[str]) -> bool:
    """Utility functino to validate if the provided scopes are sufficient"""

    for scope in required_scopes:
        if scope not in token_scopes:
            return False

    return True


async def get_current_user(
    request: Request,
    Authorize: AuthJWT = Depends(),
    db: Session = Depends(get_db),
    redis: Redis = Depends(get_cache),
):
    """Utility function that returns the user data of the user that is making the request."""

    Authorize.jwt_required()

    access_token = request.headers.get("authorization").split()[1]
    if redis.get(name=access_token):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
        )

    current_user = Authorize.get_jwt_subject()
    token_scopes = Authorize.get_raw_jwt()["scopes"]
    required_scopes = "me:read"

    authenticate_value = f'Bearer scope="{required_scopes}"'

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail={"error": {"credentials": "invalid credentials"}},
        headers={"WWW-Authenticate": authenticate_value},
    )

    user = db.query(User).filter(User.email == current_user).first()

    if not user:
        raise credentials_exception

    if not validate_scopes(
        required_scopes=required_scopes.split(" "), token_scopes=token_scopes
    ):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail={"errors": {"scopes": "insufficient permissions"}},
            headers={"WWW-Authenticate": authenticate_value},
        )

    return user
