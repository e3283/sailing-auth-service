from typing import List, Optional

from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class RefreshToken(BaseModel):
    refresh_token: str


class TokenData(BaseModel):
    email: Optional[str] = None
    scopes: List[str] = []


class Credentials(BaseModel):
    email: str
    password: str
    scopes: str
