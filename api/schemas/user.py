import datetime

from pydantic import UUID4, BaseModel, EmailStr


class UserBase(BaseModel):

    first_name: str
    last_name: str
    email: EmailStr
    consent: bool

    class Config:
        orm_mode = True


class UserOut(UserBase):

    id: UUID4
    created_at: datetime.datetime
    updated_at: datetime.datetime


class UserCreate(UserBase):
    password: str


class UserUpdate(BaseModel):

    first_name: str
    last_name: str


class UserInDB(UserCreate):
    pass
