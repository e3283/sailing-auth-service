import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy_utils.functions import create_database, database_exists

from api.database import Base, get_db, settings
from api.models import User
from api.services import get_password_hash
from main import app


@pytest.fixture
def db_engine():
    engine = create_engine(
        f"postgresql://{settings.database_username}:{settings.database_password}@{settings.database_hostname}:{settings.database_port}/auth_sailing_test"
    )
    yield engine


@pytest.fixture(scope="function")
def build_db(db_engine):

    if not database_exists(db_engine.url):
        create_database(db_engine.url)

    Base.metadata.create_all(bind=db_engine)


@pytest.fixture(scope="function")
def db(db_engine):
    connection = db_engine.connect()
    connection.begin()
    db = Session(bind=connection)

    yield db

    db.rollback()
    connection.close()


@pytest.fixture
def client(db: Session):
    app.dependency_overrides[get_db] = lambda: db

    with TestClient(app) as c:
        yield c


@pytest.fixture
def login(client: TestClient):
    def _login(
        email,
        password,
        scope="me:read competitions:read competitions:write competions:update competitions:delete",
    ):
        response = client.post(
            url="/auth/login",
            json={"email": email, "password": password, "scopes": scope},
        )

        assert response.status_code == 200

        return response.cookies.get("access_token"), response.cookies.get(
            "refresh_token"
        )

    return _login


@pytest.fixture(scope="function")
def account(db: Session):
    user = User(
        first_name="Michael",
        last_name="Groenewegen van der Weijden",
        email="michael@fragrant-chios.com",
        consent=True,
        salt="salt",
        password=get_password_hash("saltpassword"),
    )

    db.add(user)
    db.commit()
    db.refresh(user)

    yield user
